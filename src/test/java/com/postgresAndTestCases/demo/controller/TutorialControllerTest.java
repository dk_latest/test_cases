package com.postgresAndTestCases.demo.controller;


import com.postgresAndTestCases.demo.entity.Employee;
import com.postgresAndTestCases.demo.repo.EmployeeRepo;
import com.postgresAndTestCases.demo.request.EmployeeRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TutorialControllerTest {

    EmployeeRepo repo;
    @Test
    public void getAlTutorials() {

        when(repo.findAll()).thenReturn(Arrays.asList(new Employee(1l,"de","d")));
        assertEquals(1,repo.findAll().size());
    }

    @BeforeEach
    void setUp() {
        repo = mock(EmployeeRepo.class);
    }
}
