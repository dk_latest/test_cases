package com.postgresAndTestCases.demo.service;

import com.postgresAndTestCases.demo.entity.Employee;
import com.postgresAndTestCases.demo.repo.EmployeeRepo;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class EmpServiceImplTest {


    @Test
    public void getL(){
        EmployeeRepo employeeRepo=mock(EmployeeRepo.class);
        NewService newService = mock(NewService.class);
        List<Employee> list =   Arrays.asList(new Employee(1l,"de","tytytyt"),
               new Employee(1l,"kekekek","ddcsdsdsdsds"));
        when(employeeRepo.findAll()).thenReturn(list);
        when(newService.getNewList()).thenReturn(Arrays.asList("KKKK"));

        assertEquals(2,list.size());
        assertEquals(1,newService.getNewList().size());


    }


}
