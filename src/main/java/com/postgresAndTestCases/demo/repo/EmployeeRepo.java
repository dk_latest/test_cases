package com.postgresAndTestCases.demo.repo;

import com.postgresAndTestCases.demo.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee,Long> {
    List<Employee> findByName(String title);
}
