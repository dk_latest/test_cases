package com.postgresAndTestCases.demo.service;

import org.springframework.stereotype.Service;

import java.util.List;


public interface NewService {

    public List<String> getNewList();
}
