package com.postgresAndTestCases.demo.service;

import com.postgresAndTestCases.demo.entity.Employee;

import java.util.List;

public interface EmpService {
    List<Employee> getList();
}
