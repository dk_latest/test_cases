package com.postgresAndTestCases.demo.controller;


import com.postgresAndTestCases.demo.entity.Employee;
import com.postgresAndTestCases.demo.repo.EmployeeRepo;
import com.postgresAndTestCases.demo.request.EmployeeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api")
public class TutorialController {

    @Autowired
    EmployeeRepo tutorialRepository;

    @GetMapping("/health")
    public  String getHello(){
        return "Hello";
    }

    @GetMapping("/listEmployee")
    public ResponseEntity<List<Employee>> getAllTutorials(@RequestParam(required = false) String title) {
        try {
            List<Employee> tutorials = new ArrayList<Employee>();

                tutorialRepository.findAll().forEach(tutorials::add);

            if (tutorials.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/tutorials/{id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
        try {
            tutorialRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/tutorials")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        try {
            tutorialRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/addRequest")
    public ResponseEntity<Employee> createTutorial(@RequestBody EmployeeRequest request) {
        try {
            Employee employee = tutorialRepository
                    .save(Employee.builder().city(request.getCity()).name(request.getName()).build());
            return new ResponseEntity<>(employee, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/getList")
    public ResponseEntity<List<Employee>> getList() {
        try {


            List<Employee> tutorials = new ArrayList<Employee>();

            tutorialRepository.findAll().forEach(tutorials::add);

            if (tutorials.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
